﻿using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public interface Trader_I : CanHaul_I
	{
		List<Tradable_I> getTradables();
		List<Tradable_I> getObjectsToTrade();
		Agent getAgent();
	}
}
