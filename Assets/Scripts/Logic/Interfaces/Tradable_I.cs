﻿using UnityEngine;

namespace XTrade
{
	public interface Tradable_I : Haulable_I
	{
		int getAmount();
	}

	public enum WareType
	{
		IRON_ORE = 15,
		FOOD = 20,
		WATER = 20,
		STEEL = 70,
		ELECTRONICS = 180,
		FIGHTER = 5000,
		HAULER = 10000,
		MINER = 7500,
		STATION = 10000
	}
}
