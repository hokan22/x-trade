﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouseController : MonoBehaviour 
{
	// Use this for initialization
	void Start() 
	{
	}

	bool isDraggingCamera = false;
	Vector3 lastMousePosition;

	// Speed Settings
	public float movementSpeed = 40f;
	public float scrollSpeed = 2f;

	// Update is called once per frame
	void Update() 
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			// Mouse Button Down start drag
			isDraggingCamera = true;

			lastMousePosition = Input.mousePosition;
		} else if (Input.GetMouseButtonUp(0)) 
		{
			// Mouse Button went UP stop drag
			isDraggingCamera = false;
		}
			
		// Camera Drag Function
		if (isDraggingCamera) 
		{
			// Movement of Camera
			Vector3 diff = lastMousePosition - Input.mousePosition;

			// movementSpeed = -2.36f * Camera.main.orthographicSize + 56.44f;
			Camera.main.transform.Translate (diff / movementSpeed, Space.World);
			lastMousePosition = Input.mousePosition;
		}

		float scrollAmount = Input.GetAxis ("Mouse ScrollWheel");
		if (Mathf.Abs (scrollAmount) > 0.01f) 
		{
			Transform BackgroundTransform =  GetComponentsInChildren<Transform>()[1];
			
			Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize + scrollAmount * scrollSpeed, 4, 20);

			float scale = Camera.main.orthographicSize * 5 + 20;

			BackgroundTransform.localScale = new Vector3 (scale, scale, 1);
		}
	}
}
