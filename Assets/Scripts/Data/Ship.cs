﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class Ship : SpaceEntity
	{
		private static int ID = 0;
		private int id;
		private int price { get; set; }
		private float timeToMineMillis;


		// State of Ship and NextState for Movement Finish
		private ShipState State = ShipState.IDLING;
		public ShipState pubState { get { return State; } }
		private ShipState NextState;

		// Movement Variables
		private SpaceEntity Target;
		private Vector3 targetPosition;
		private Vector3 currentPosition;
		private Vector3 directionOfTravel;

		public float reach = 1f;
		public float speed = 1f;
		public int mineSpeed = 50;

		public Ship()
		{
		}

		public new void Awake()
		{
			base.Awake();
			id = ID;
			ID++;

            mineSpeed = 50;
		}

        // Method to give Orders to the ship (state machine)
        internal bool orderTask(Task Task, WareType WareType)
        {
            // Search for nearest GameObject in WorldSpace from Ship
            this.Target = SearchNearest(WareType);

			// Check if a SpaceEntity with WareType was found, 
			// else return that the Task could not be executed (false)
			// Currently a valid Target is required for all implemented Tasks
            if (this.Target == null)
            {
				Debug.Log("No suitable Target was found");
                return false;
            }

			// Set State to the given Task
			switch (Task)
			{
			case Task.MINE:

				Asteroid asteroidTarget = (Asteroid)this.Target;
				State = ShipState.MINING;
				timeToMineMillis = asteroidTarget.Amount / mineSpeed;
				break;

			case Task.FLYTO:

				State = ShipState.MOVING;
				break;

				// Catch all unhandled TaskTypes and return that the Task could not be executed
			default:
				return false;
			}

			// Return true for sucessfully giving order
			return true;
		}

        
        private SpaceEntity SearchNearest(WareType WareType) 
        {
            // Setup return value
            SpaceEntity nearestSpaceEntity = null;

			// Get all SpaceEntitys in WorldSpace
			SpaceEntity[] allSpaceEntitys = GameObject.FindObjectsOfType<SpaceEntity>();

			//Debug.Log (allSpaceEntitys.Length + " were found");

			// Iterate through every SpaceEntity in Worldspace
			foreach (SpaceEntity SpaceEntity in allSpaceEntitys)
			{

				//Debug.Log ("Entity: " + SpaceEntity);
				//Debug.Log ("WareType: " + SpaceEntity.getWareType());
				// Skip further checks when SpaceEntity is not from given WareType
				if (SpaceEntity.getWareType() != WareType)
				{
					continue;
				}

				// Set first found SpaceEntity to be the nearest
				if (nearestSpaceEntity == null)
				{
					if(SpaceEntity != null && ((Asteroid) SpaceEntity).BeingMinedBy != null)
					{
						continue;
					}

					nearestSpaceEntity = SpaceEntity;
					continue;
				}

				// Check if SpaceEntity is closer to this.Ship and update nearestSpaceEntity accordingly
				if (Vector3.Distance(this.transform.position, SpaceEntity.transform.position) <= Vector3.Distance(this.transform.position, nearestSpaceEntity.transform.position))
				{
					if (SpaceEntity != null && ((Asteroid)SpaceEntity).BeingMinedBy != null)
					{
						continue;
					}
					nearestSpaceEntity = SpaceEntity;
				}
			}

			//Debug.Log ("Nearest: " + nearestSpaceEntity);
			return nearestSpaceEntity;
		}


        void Mine() 
		{
			// Check if the Target is interactable
			if (this.Target is Interactable_I && this.Target.getInteractOptions ().Contains (Interactions.MINEABLE)) {

				// This will get called every to update the position during movement
				// Get World Position of Target 
				targetPosition = this.Target.transform.position;
				// Get World Position of this Object
				currentPosition = this.transform.position;

				Asteroid asteroidTarget = (Asteroid)this.Target;
				if (asteroidTarget.BeingMinedBy != null && this.id != asteroidTarget.BeingMinedBy.id)
				{
					this.State = ShipState.IDLING;
					return;
				}

				if (Vector3.Distance (currentPosition, targetPosition) > reach + 1) {
					Debug.Log ("Target not in Range");
					// IF NOT IN RANGE
					this.NextState = ShipState.MINING;
					this.State = ShipState.MOVING;
					return;
				}

				asteroidTarget.BeingMinedBy = this;

				timeToMineMillis -= Time.deltaTime ;

				if (timeToMineMillis <= 0)
				{
					actualMining();
					Debug.Log("*MINING*");
					((Asteroid)Target).wasMined();
					this.State = ShipState.IDLING;
				}

				//Destroy (Target.gameObject);

				//this.State = ShipState.IDLING;


			} else 
			{
				//Debug.Log ("Cannot Mine");
				//Debug.Log ("Mineable: " + this.Target.getInteractOptions ().Contains (Interactions.MINEABLE));
				//Debug.Log ("Interactable: " + (this.Target is Interactable_I));

				// Mining at Target is not possible, so set Ship State back to Idle
				this.State = ShipState.IDLING;
			}
		}

		private void actualMining()
		{
			Asteroid roidToMine = (Asteroid)this.Target;
			Ware minedWare = new Ware(roidToMine.getWareType(), roidToMine.getAmount());
			addFreight(minedWare);

		}

        private void Move() 
        {
			// This will get called every to update the position during movement
			// Get World Position of Target Object
			targetPosition = this.Target.transform.position;
			// Get World Position of this Object
			currentPosition = this.transform.position;
			// Calculate Direction where to move to
			directionOfTravel = targetPosition - currentPosition;

			//Debug.Log ("DisNR: " + Vector3.Distance (currentPosition, targetPosition));

			// Calculate targetPosition offset with reach of ship, to get fadeout movement
			targetPosition = targetPosition + (Vector3.Normalize(directionOfTravel * -1)) * reach;

			//Debug.Log ("DisR: " + Vector3.Distance (currentPosition, targetPosition));

			// Recalculate Direction based on calculated TargetPosition
			directionOfTravel = Vector3.Normalize(targetPosition - currentPosition);

			// Only move when Target is out of Reach
			if (Vector3.Distance(currentPosition, targetPosition) > 0.1f)
			{
				//Debug.Log ("Moving");
				//Debug.Log ("X: " + directionOfTravel.x * speed * Time.deltaTime);

				// Calculate new Position based on speed and deltaTime between frames
				this.transform.Translate(
					(directionOfTravel.x * speed * Time.deltaTime),
					(directionOfTravel.y * speed * Time.deltaTime),
					(directionOfTravel.z * speed * Time.deltaTime),
					Space.World);
			}
			// If target is in range end movement and set to next ShipState
			else
			{
				this.State = this.NextState;
				this.NextState = ShipState.IDLING;
			}    
		}

        /// Update is called every frame, if the MonoBehaviour is enabled.
        void Update()
        {
			if (this.State == ShipState.IDLING)
			{
				Debug.Log("Ship Idling");
				return;
			}
            else if (this.Target != null) {    
               
                if (this.State == ShipState.MOVING)
                {
                    this.Move();
                } 
                
                else if (this.State == ShipState.MINING)
                {
                    this.Mine();
                }
            }   
            else
            {
                Debug.Log("Ship with no state");
                this.State = ShipState.IDLING;
            }
        }
    }

	// Current Action of Ship
	public enum ShipState
	{
		IDLING,
		MOVING,
		MINING,
		FIGHTING,
		FLEEING,
	}

	// AI Operation to control Ship
	public enum Task
	{
		FLEE = 0,
		MINE = 1,
		FLYTO = 2
	}
}
