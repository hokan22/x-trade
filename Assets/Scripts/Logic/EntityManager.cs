﻿using System.Collections.Generic;
using System;

namespace XTrade
{
    public class EntityManager
    {
        private Dictionary<Type, List<SpaceEntity>> allEntities;
        public Dictionary<Type, List<SpaceEntity>> AllEntities { get { return allEntities; } set { allEntities = value; } }

        public EntityManager()
        {
            allEntities = new Dictionary<Type, List<SpaceEntity>>();
        }

        public Boolean hasEntityOfType(Type entityType)
        {
            if (allEntities.ContainsKey(entityType))
            {
                return true;
            }

            return false;
        }

        public List<SpaceEntity> getAllEntitiesOfType(Type entityType)
        {
            return allEntities[entityType];
        }

        public void addEntities(Type type, List<SpaceEntity> listOfEntities)
        {
            allEntities.Add(type, listOfEntities);
        }

        public void addEntity(SpaceEntity entity)
        {
            if (hasEntityOfType(entity.GetType()))
            {
                List<SpaceEntity> entitiesOfType = getAllEntitiesOfType(entity.GetType());
                entitiesOfType.Add(entity);
            }
            else
            {
                addEntities(entity.GetType(), new List<SpaceEntity> { entity });
            }
        }
    }
}
