﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public interface CanHaul_I
	{
		List<Haulable_I> getFreight();
		List<Haulable_I> determinFreightToExchange();
		void addFreight(List<Haulable_I> newFreight);
	}
}
