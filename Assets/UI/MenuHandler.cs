﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace XTrade
{
	public class MenuHandler : MonoBehaviour 
    {
	 	public GameObject menu;

		private Ship parentShip;

		// Start is called on the frame when a script is enabled just before
		// any of the Update methods is called the first time.
		void Start()
		{
			menu.SetActive(false);
			parentShip = GetComponent<Ship>();
		}
		
		// Update is called every frame, if the MonoBehaviour is enabled.
		void Update()
		{
		}

		// Called every frame while the mouse is over the GUIElement or Collider.
		void OnMouseOver()
		{
			if (parentShip.Owner.GetType() == typeof(Player) ) 
			{
				// Detect Right Mouse pressed
				if (Input.GetMouseButton(0)) 
				{
					// Display RightClickMenu
					menu.SetActive(true);
				}
			}
		}

		public void mineIron()
		{
			parentShip.orderTask(Task.MINE, WareType.IRON_ORE);
			menu.SetActive(false);
		}

		public void mineWater()
		{
			parentShip.orderTask(Task.MINE, WareType.WATER);
			menu.SetActive(false);
		}
	}
}
