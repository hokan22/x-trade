﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class Asteroid : SpaceEntity
	{
		private int amount;
		public int Amount { get { return amount; } set { amount = value; } }

		private Ship beingMinedBy;
		public Ship BeingMinedBy { get { return beingMinedBy; } set { beingMinedBy = value; } }

		List<Interactions> Interactions;

		// Constructor
		public Asteroid()
		{
		}

		public new void Awake()
		{
			Interactions = new List<Interactions>
			{
				XTrade.Interactions.MINEABLE
			};
		}

		public List<Interactions> getInteractOptions()
		{
			return this.Interactions;
		}

		public new int getAmount()
		{
			return this.Amount;
		}

		public void wasMined()
		{
			Destroy (this.gameObject);
			Debug.Log ("Asteroid was mined");
		}
	}
}
