﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace XTrade
{
    public class ScoreList : MonoBehaviour
    {
        // Element to spawn as a List Item.
        // The List expects tje List Item to have certainly named Text Components for Display
        public GameObject scoreListPrefab;

        ScoreManager scoreManager;

        // Use this for initialization
        void Start()
        {
            // Find and save scoreManager for later use
            scoreManager = GameObject.FindObjectOfType<ScoreManager>();
        }

        // Update is called once per frame
        void Update()
        {

            // Only update Score List when score has actually changed to improve performance        
            if (scoreManager.getHasChanged() == false)
            {
                return;
            }
            scoreManager.setHasChanged(false);

            // Remove all current Score List Entrys from UI
            while (this.transform.childCount > 0)
            {
                Transform c = this.transform.GetChild(0);
                c.SetParent(null);

                Destroy(c.gameObject);
            }

            // Get all registered Agent Names and create a ScoreListObject for each
            string[] names = scoreManager.getAgentNames();

            foreach (string name in names)
            {
                // Spawn ListObject and Attach to ScoreList
                // This ListObject holds Elements which hold the Text actually displayed to the User
                GameObject go = Instantiate(scoreListPrefab);
                go.transform.SetParent(this.transform);

                // Change Text according to the Values
                // The User can identify these by a statically set Header in the UI List Element
                go.transform.Find("AgentName").GetComponent<Text>().text = name;
                go.transform.Find("Iron").GetComponent<Text>().text = scoreManager.getScore(name, "IRON_ORE").ToString();
                go.transform.Find("Water").GetComponent<Text>().text = scoreManager.getScore(name, "WATER").ToString();
                go.transform.Find("Steel").GetComponent<Text>().text = scoreManager.getScore(name, "STEEL").ToString();
                go.transform.Find("Food").GetComponent<Text>().text = scoreManager.getScore(name, "FOOD").ToString();
                go.transform.Find("Electronic").GetComponent<Text>().text = scoreManager.getScore(name, "ELECTRONIC").ToString();
                go.transform.Find("Money").GetComponent<Text>().text = scoreManager.getScore(name, "MONEY").ToString();
            }
        }
    }
}
