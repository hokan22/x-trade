﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class TradeContract
	{
		private Agent owner;
		public Agent Owner
		{
			get { return owner; }
			set { owner = value; }
		}

		private Haulable_I tradeGood;
		public Haulable_I TradeGood
		{
			get { return tradeGood; }
			set { tradeGood = value; }
		}

		private WareType wareType;
		public WareType WareType
		{
			get { return wareType; }
		}

		private int pricePerUnit;
		public int PricePerUnit
		{
			get { return pricePerUnit; }
			set { pricePerUnit = value; }
		}

		public int Price
		{
			get { return pricePerUnit * amount; }
		}

		private int amount;
		public int Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		public TradeContract(Agent owner, Haulable_I typeOfGood, int pricePerUnit, int amount)
		{
			this.owner = owner;
			tradeGood = typeOfGood;
			wareType = typeOfGood.getWareType();
			this.pricePerUnit = pricePerUnit;
			this.amount = amount;
		}
	}
}
