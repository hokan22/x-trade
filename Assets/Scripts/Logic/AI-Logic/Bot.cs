﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{

	public class Bot : Agent
	{
		public GameObject StationPref;

		public Bot()
		{
		}

		public void Awake()
		{
			base.Awake();
		}

		public void assesSituation()
		{
			//canBuildStation?
			if (Wallet > 10000 && hasMoreThanThresholdOfWare(100, WareType.STEEL) && hasMoreThanThresholdOfWare(100, WareType.ELECTRONICS))
			{
				Debug.Log("BuildStation");
				buildStation();
				return;
			}
			else
			{

				if (hasMoreThanThresholdOfWare(100, WareType.STEEL))
				{
					setStaionBoolForAll(WareType.STEEL, false);
					if (Wallet < 10000)
					{

					}
				}
				else
				{
					designateShipTask(WareType.STEEL);
				}
				if (hasMoreThanThresholdOfWare(100, WareType.ELECTRONICS))
				{
					setStaionBoolForAll(WareType.ELECTRONICS, false);
					if (Wallet < 10000)
					{

					}
				}
				else
				{
					designateShipTask(WareType.ELECTRONICS);
				}
			}
		}

		private void setStaionBoolForAll(WareType wareType, bool state)
		{
			List<SpaceEntity> stationList = EntityManager.getAllEntitiesOfType(typeof(Station));

			foreach(SpaceEntity station in stationList)
			{
				Station actualStation = (Station)station;
				actualStation.setProductionBool(wareType, state);
			}
		}

		private void designateShipTask(WareType wareType)
		{
			Debug.Log("Looking for Ship");
			List<SpaceEntity> shipList = EntityManager.getAllEntitiesOfType(typeof(Ship));

			// This block checks if there are Idle Miningships and assigns a Task to them
			foreach (SpaceEntity entity in shipList)
			{
				Ship ship = (Ship)entity;
				Debug.Log("Ship Entity has been Set!");
				if (ship.pubState == ShipState.IDLING && ship.getWareType() == WareType.MINER)
				{

					determineShipBehaviour(ship, Task.MINE, wareType);

				}
			}
		}

		private Boolean hasMoreThanThresholdOfWare(int threshold, WareType waretype)
		{
			int wareAmount = 0;
			foreach(Haulable_I haulable in TotalFreight)
			{
				if(haulable.getWareType() == waretype)
				{
					Ware steel = (Ware)haulable;
					wareAmount += steel.getAmount();
				}
			}

			return wareAmount > threshold ? true : false;
		}

		public int howMuchWareOfType(WareType waretype)
		{
			int wareAmount = 0;
			foreach (Haulable_I haulable in TotalFreight)
			{
				if (haulable.getWareType() == waretype)
				{
					Ware steel = (Ware)haulable;
					wareAmount += steel.getAmount();
				}
			}

			return wareAmount;
		}


		private void determineShipBehaviour(Ship ship)
		{

			switch (ship.WareType)
			{
			case WareType.MINER:

				break;
			case WareType.HAULER:

				break;
			case WareType.FIGHTER:

				break;
			}

		}

		private void sellResource(int thresholdNotToUndermine, WareType wareToSell)
		{
			// This block designates all Wares of the param Waretype and counts the total Number
			int wareAmount = 0;
			List<Ware> listOfWareToSell = new List<Ware>();
			foreach (Haulable_I haulable in TotalFreight)
			{
				if (haulable.getWareType() == wareToSell)
				{
					Ware ware = (Ware)haulable;
					wareAmount += ware.getAmount();


					listOfWareToSell.Add(ware);
				}
			}

			int amountToSell = wareAmount - thresholdNotToUndermine;

			// This block cycles through the Wares of the Type to sell and sells them

			foreach(Ware ware in listOfWareToSell)
			{
				//If the amountToSell exceeds the Amount of the current Ware, it sells the whole object
				if(amountToSell > ware.getAmount()){
					TradeMaster.sellWare(ware, (Agent) this);
					amountToSell -= ware.getAmount();
					TotalFreight.Remove(ware);
				}
				// If the amountToSell does not exceed the Amount of the current Ware, it create a new Ware object, which holds the delta Amount of 
				// the ware to Sell and sells the current ware object.
				else
				{
					Ware deltaWare = new Ware(ware.getWareType(), ware.getAmount() - amountToSell);
					ware.reduceAmountBy(ware.getAmount() - amountToSell);
					amountToSell = 0;
					TradeMaster.sellWare(ware, (Agent)this);
					TotalFreight.Remove(ware);
					TotalFreight.Add(deltaWare);
				}

			}

		}

		public override Decisions determineMarketplaceDecision()
		{
			throw new System.NotImplementedException();
		}

		public override List<Tradable_I> determineObjectsToTrade(List<Tradable_I> tradableObjects)
		{
			throw new System.NotImplementedException();
		}

		internal override TradeContract determineContractToBuy(List<TradeContract> contracts)
		{
			throw new System.NotImplementedException();
		}

		internal override int determineContractWareAmount()
		{
			throw new System.NotImplementedException();
		}

		internal override Haulable_I determineContractWareType()
		{
			throw new System.NotImplementedException();
		}

		internal override List<Haulable_I> determineFreightForExchange()
		{
			throw new System.NotImplementedException();
		}

		internal override List<Haulable_I> determinFreightForExchange(List<Haulable_I> freight)
		{
			throw new System.NotImplementedException();
		}

		internal override void buildStation()
		{
			System.Random rnd = new System.Random();
			int x = rnd.Next(0, 50);
			int y = rnd.Next(0, 50);

			GameObject stationGO = Instantiate(StationPref);
			stationGO.transform.Translate(new Vector3(x, y + 2, 0));
			Station station = (Station)stationGO.GetComponent(typeof(Station));
			station.WareType = WareType.STATION;
			station.Awake();
			station.Owner = this;

			Wallet -= 10000;
			reduceWareBy(WareType.STEEL, 100);
			reduceWareBy(WareType.ELECTRONICS, 100);
		}

		internal override void determineShipBehaviour(Ship ship, Task TASK, WareType WARETYPE)
		{
			switch (TASK)
			{
			case Task.MINE:
				determineMineTask(ship, WARETYPE);
				break;
			}
		}

		private void determineMineTask(Ship ship, WareType WARETYPE)
		{
			Debug.Log("sending ship to Mine "+  ship.WareType.ToString());
			switch (WARETYPE)
			{
			case WareType.STEEL:
				setStaionBoolForAll(WareType.STEEL, true);
				setStaionBoolForAll(WareType.FOOD, true);
				if (hasMoreThanThresholdOfWare(20, WareType.WATER))
				{
					if (hasMoreThanThresholdOfWare(20, WareType.FOOD))
					{
						setStaionBoolForAll(WareType.FOOD,false);
						Debug.Log("orderTaskSTeel");
						ship.orderTask(Task.MINE, WareType.IRON_ORE);
					}
					else
					{

						ship.orderTask(Task.MINE, WareType.WATER);
						Debug.Log("FoodBreak");
						break;
					}
				}
				else
				{
					Debug.Log("orderTaskWater");
					ship.orderTask(Task.MINE, WareType.WATER);
				}
				break;

			case WareType.ELECTRONICS:
				setStaionBoolForAll(WareType.ELECTRONICS, true);
				setStaionBoolForAll(WareType.FOOD, true);

				if (hasMoreThanThresholdOfWare(20, WareType.WATER))
				{
					if(hasMoreThanThresholdOfWare(20, WareType.FOOD))
					{
						setStaionBoolForAll(WareType.FOOD, false);
						if (hasMoreThanThresholdOfWare(200, WareType.STEEL))
						{
							setStaionBoolForAll(WareType.STEEL, false);
							Debug.Log("SteelBreak");
							// If there is enough steel, water and food the Bot just needs to wait for the productioncycle
							break;
						}
						else
						{
							setStaionBoolForAll(WareType.STEEL, true);
							Debug.Log("orderTaskSTeel2");
							ship.orderTask(Task.MINE, WareType.IRON_ORE);
						}
					}
					else
					{
						Debug.Log("FoodBreak");
						break;
					}
				}
				else
				{
					Debug.Log("orderTaskWater");
					ship.orderTask(Task.MINE, WareType.WATER);
				}


				break;
			}
		}

		private void determineTradeDecision()
		{
			throw new NotImplementedException();
		}
	}
}
