﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class Ware : Tradable_I, Haulable_I
	{
		private WareType wareType;
		private int amount;

		public Ware(WareType type, int amount)
		{
			wareType = type;
			this.amount = amount;
		}

		public int getAmount()
		{
			return amount;
		}

		public void reduceAmountBy(int amountToReduce)
		{
			amount -= amountToReduce;
		}

		public WareType getWareType()
		{
			return wareType;
		}
	}
}
