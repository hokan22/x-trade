﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class SimulationMaster : MonoBehaviour
	{
		float tickTime = 1;
		float roidSpawnTick = 5;
		float debugTick = 10;
		static List<Bot> playerEntities;

		public GameObject AsteroidPref;
		public GameObject StationPref;
		public GameObject ShipPref;
		public GameObject BotPref;

		void Update()
		{


			if (tickTime <= 0)
			{
				tickTime = 1;

				if (playerEntities != null)
				{
					foreach (Bot player in playerEntities)
					{
						if (player.hasFiveStation())
						{
							return;
						}
						player.assesSituation();


					}
					if(debugTick <= 0)
					{
						debugTick = 10;
						foreach (Bot player in playerEntities)
						{
							Debug.Log("Money: " + player.Wallet + " Electronics: " +player.howMuchWareOfType(WareType.ELECTRONICS)+ " Steel: " + player.howMuchWareOfType(WareType.STEEL) + " Food: " + player.howMuchWareOfType(WareType.FOOD) + " Water: "+player.howMuchWareOfType(WareType.WATER));

						}
					}
				}
			}
			else if(roidSpawnTick <= 0)
			{
				Debug.Log("spawnRoid");
				roidSpawnTick = 5;
				spawnRoid(new System.Random());
			}

			tickTime -= Time.deltaTime;
			roidSpawnTick -= Time.deltaTime;
			debugTick -= Time.deltaTime;
		}


		// Initiates the Application with default-values and starts the Backgroundsimulation
		public new void Awake()
		{
			System.Random rnd = new System.Random();

			for (int i = 0; i < 250; i++)
			{
				spawnRoid(rnd);
			}

			playerEntities = new List<Bot>();

			Debug.Log("SimulationMaster Awake");

			for(int i = 0; i < 3; i++)
			{

				int x = rnd.Next(0, 250);
				int y = rnd.Next(0, 250);

				Debug.Log("For runde " + i);
				GameObject botGO = Instantiate(BotPref);

				Bot botPlayer = (Bot) botGO.GetComponent(typeof(Bot));

				botPlayer.Awake();
				botPlayer.StationPref = this.StationPref;


				GameObject miner1GO = Instantiate(ShipPref);
				miner1GO.transform.Translate(new Vector3(x, y, 0));
				Ship miner1 = (Ship)miner1GO.GetComponent(typeof(Ship));
				miner1.WareType = WareType.MINER;
				miner1.Awake();
				miner1.Owner = botPlayer;

				GameObject stationGO = Instantiate(StationPref);
				stationGO.transform.Translate(new Vector3(x, y+2, 0));
				Station station = (Station)stationGO.GetComponent(typeof(Station));
				station.WareType = WareType.STATION;
				station.Awake();
				station.Owner = botPlayer;

				Debug.Log("Before ENtityManager");
				botPlayer.EntityManager.addEntities(typeof(Ship), new List<SpaceEntity> { miner1});
				botPlayer.EntityManager.addEntity(station);

				Debug.Log("For Ende");

				playerEntities.Add(botPlayer);


			}


		}

		public void buildStationFor(Agent agent)
		{


		}

		//initiates the marketplace of a specified station for a specified trader(ship)
		//the Marketplace will be open for the ship till the Agent decides to leave it
		public void initMarketplace(Station station, Trader_I trader)
		{

			TradeManager marketplace = station.getTradeManager();
			Agent tradeAgent = trader.getAgent();

			Decisions decision;

			do { 

				//this object represents the decision of the Agent as an enum.
				decision = tradeAgent.determineMarketplaceDecision();

				if (decision.Equals(Decisions.NEW_CONTRACT))
				{
					marketplace.Contracts.Add(TradeMaster.createNewTradeContract(tradeAgent));
				}
				else if (decision.Equals(Decisions.BUY_CONTRACT))
				{
					TradeContract contractToBuy = tradeAgent.determineContractToBuy(marketplace.Contracts);
					marketplace.Contracts.Remove(contractToBuy);

					// Transfers the tradebalance from the buyer.Agent to the seller.Agent
					TradeMaster.doTradeBalance(contractToBuy.Owner, trader.getAgent(), contractToBuy);

					// Transfers the physical goods from CanHaul_IOne(Station) to CanHaul_ITwo(Ship)
					TradeMaster.doFreightExchangeContract(station, trader, contractToBuy);
				}

			}while(!decision.Equals(Decisions.LEAVE_MARKETPLACE));


		}

		// tunnels the freightExchange to the TradeMaster
		public void initFreightExchange(CanHaul_I haulerOne, CanHaul_I haulerTwo)
		{
			TradeMaster.doFreightExchange(haulerOne, haulerTwo);
		}


		private void spawnRoid(System.Random rnd)
		{

			int x = rnd.Next(0, 250);
			int y = rnd.Next(0, 250);

            // Spawn Asteroids and attach to Asteroid Master Object to keep SceneInspector clean (no impact on Gameplay)
			GameObject asteroidGO = Instantiate(AsteroidPref);
            asteroidGO.transform.SetParent(GameObject.Find("Asteroids").transform);

            Asteroid asteroid = (Asteroid)asteroidGO.GetComponent(typeof(Asteroid));


			if (rnd.Next(0, 5) < 2)
			{
				asteroid.WareType = WareType.IRON_ORE;
				asteroid.Amount = rnd.Next(15, 45);
			}
			else
			{
				asteroid.WareType = WareType.WATER;
				asteroid.Amount = rnd.Next(50, 150);
			}

			asteroidGO.transform.Translate(new Vector3(x, y, 0));


		}


	}
}
