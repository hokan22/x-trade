﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class TradeMaster
	{
		public static TradeContract createNewTradeContract(Agent tradeAgent)
		{
			//Lets the Agent determine which Ware at which Price he is going to create a contract for.
			Haulable_I tradeGood = tradeAgent.determineContractWareType();
			int pricePerUnit = TradeMaster.getCurrentPrice(tradeGood.getWareType());
			int amount = tradeAgent.determineContractWareAmount();

			return new TradeContract(tradeAgent, tradeGood, pricePerUnit, amount);
		}

		//This method will care for every transaction between Agents.
		internal static void doTradeBalance(Agent seller, Agent buyer, TradeContract contract)
		{
			buyer.Wallet -= contract.Price;
			seller.Wallet += contract.Price;
		}

		//This method takes two HaulerObjects and asks their Agents to determin which freight they want to exchange. After that it exchanges the freight from the two Haulers
		internal static void doFreightExchange(CanHaul_I hauler1, CanHaul_I hauler2)
		{
			// Deletes the Cargo from the hauler and adds it to the hauler.
			// Todo: Cargohold size failcheck, if freight is to large for the ship.
			List<Haulable_I> hauler1Freight = hauler1.determinFreightToExchange();
			List<Haulable_I> hauler2Freight = hauler2.determinFreightToExchange();

			foreach (Haulable_I freight in hauler1Freight)
			{
				hauler1.getFreight().Remove(freight);
				hauler2.addFreight(new List<Haulable_I> { freight });
			}

			foreach (Haulable_I freight in hauler2Freight)
			{
				hauler2.getFreight().Remove(freight);
				hauler1.addFreight(new List<Haulable_I> { freight });
			}

		}

		internal static void doFreightExchangeContract(CanHaul_I hauler1, CanHaul_I hauler2, TradeContract contractToBuy)
		{
			// Deletes the Cargo from the station and adds it to the ship.
			// Todo: Cargohold size failcheck, if freight is to large for the ship.
			hauler1.getFreight().Remove(contractToBuy.TradeGood);
			hauler2.addFreight(new List<Haulable_I> { contractToBuy.TradeGood });
		}

		internal static int getCurrentPrice(WareType wareType)
		{
			throw new NotImplementedException();
		}

		internal static void sellWare(Ware ware, Agent agent)
		{
			int currentWarePrice = TradeMaster.getCurrentPrice(ware.getWareType());

			agent.Wallet += currentWarePrice * ware.getAmount();
		}
	}
}
