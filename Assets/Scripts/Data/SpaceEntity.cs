﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace XTrade
{
	public abstract class SpaceEntity : MonoBehaviour, Ownable_I, Interactable_I, Trader_I, Tradable_I
	{

		private Agent owner;
		public Agent Owner { get { return owner; } set { owner = value; } }

		private WareType wareType;
		public WareType WareType { get { return wareType; } set { wareType = value; } }


		List<Haulable_I> freight;

		public List<Interactions> InteractOptions;


		public SpaceEntity()
		{
		}

		public void Awake()
		{
			freight = new List<Haulable_I>();
		}

		public int getAmount()
		{
			// solange nicht stackable!!! muss eventuell geändert werden.
			return 1;
		}

		public List<Tradable_I> getObjectsToTrade()
		{
			return owner.determineObjectsToTrade(getTradables());
		}

		public List<Tradable_I> getTradables()
		{
			List<Tradable_I> tradeGoods = new List<Tradable_I>();
			foreach(Haulable_I haulable in freight)
			{
				if(haulable is Tradable_I)
				{
					tradeGoods.Add((Tradable_I)haulable);
				}
			}

			return tradeGoods;
		}

		public WareType getWareType()
		{
			return wareType;
		}


		public string getWareTypeNiceName()
		{
			switch (wareType) 
			{
			case WareType.IRON_ORE:
				return "Iron Ore";	

			default:
				return wareType.ToString();
			}

		}

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		public List<Haulable_I> determinFreightToExchange()
		{
			List<Haulable_I> freightForExchange = owner.determinFreightForExchange(freight);
			return freightForExchange;
		}

		public List<Haulable_I> getFreight()
		{
			List<Haulable_I> Freight = new List<Haulable_I>();
			Freight.AddRange(freight);
			freight = new List<Haulable_I>();

			return Freight;
		}

		public void addFreight(List<Haulable_I> newFreight)
		{
			freight.AddRange(newFreight);
		}

		public void addFreight(Haulable_I newFreight)
		{
			if(freight == null)
			{
				freight = new List<Haulable_I>();
			}
			freight.Add(newFreight);
		}

		public Agent getAgent()
		{
			return owner;
		}

		public List<Interactions> getInteractOptions()
		{
			return this.InteractOptions;
		}
	}
}
