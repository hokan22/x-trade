﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class Station : SpaceEntity, HasMarketplace_I
	{
		private TradeManager marketPlace;
		private float productionTick = 5;
		private float moneyTick = 30;
		private bool produceFood = true;
		private bool produceSteel = true;
		private bool produceElectronics = true;

		public Station()
		{
		}

		public new void Awake()
		{
			base.Awake();
		}

		public TradeManager getTradeManager()
		{
			return marketPlace;
		}

		// Use this for initialization
		void Start()
		{

		}

		private bool hasMoreThanThresholdOfWare(int threshold, WareType waretype)
		{
			int wareAmount = 0;
			foreach (Haulable_I haulable in Owner.TotalFreight)
			{
				if (haulable.getWareType() == waretype)
				{
					Ware steel = (Ware)haulable;
					wareAmount += steel.getAmount();
				}

			}

			return wareAmount > threshold ? true : false;
		}

		public void  setProductionBool(WareType wareType,bool state)
		{
			switch (wareType)
			{
			case WareType.STEEL:
				produceSteel = state;
				break;

			case WareType.ELECTRONICS:
				produceElectronics = state;
				break;

			case WareType.FOOD:
				produceFood = state;
				break;
			}
		}

		// Update is called once per frame
		void Update()
		{
			if (productionTick <= 0)
			{
				productionTick = 1;

				if (hasMoreThanThresholdOfWare(5, WareType.WATER) && produceFood)
				{
					Debug.Log("adding FOOD");
					Owner.reduceWareBy(WareType.WATER, 5);
					this.addFreight(new Ware(WareType.FOOD, 5));

				}
				if (hasMoreThanThresholdOfWare(5, WareType.WATER) && hasMoreThanThresholdOfWare(5, WareType.FOOD) && hasMoreThanThresholdOfWare(5, WareType.IRON_ORE) && produceSteel)
				{
					Debug.Log("adding STEEL");
					Owner.reduceWareBy(WareType.WATER, 5);
					Owner.reduceWareBy(WareType.FOOD, 5);
					Owner.reduceWareBy(WareType.IRON_ORE, 5);
					this.addFreight(new Ware(WareType.STEEL, 5));

				}
				if (hasMoreThanThresholdOfWare(5, WareType.WATER) && hasMoreThanThresholdOfWare(5, WareType.FOOD) && hasMoreThanThresholdOfWare(5, WareType.STEEL)&& produceElectronics)
				{
					Debug.Log("adding ELECTRONICS");
					Owner.reduceWareBy(WareType.WATER, 5);
					Owner.reduceWareBy(WareType.FOOD, 5);
					Owner.reduceWareBy(WareType.STEEL, 5);

					this.addFreight(new Ware(WareType.ELECTRONICS, 5));
				}


			}
			if(moneyTick <= 0)
			{
				moneyTick = 10;
				this.Owner.Wallet += 500;
			}

			productionTick -= Time.deltaTime;
			moneyTick -= Time.deltaTime;

		}
	}
}
