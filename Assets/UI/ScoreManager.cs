﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace XTrade
{
    public class ScoreManager : MonoBehaviour {

        // Dictionary to Score the Scores in.
        // This is a 2D Dictionary ([Agentname => [scoreType => Score]])
        // This Dictionary is not confined to a certain range of ScoreTypes, but rather saves these string based.
        // That means the ScoreManager is capable to score any kind of ScoreType and also return it.
        private Dictionary<string, Dictionary<string, int>> agentScores;

        // bool for custom List update Trigger used in ScoreList
        private bool hasChanged = false;
        // Reference to all Agents in play
        private Agent[] allAgents;

        // Gets called for each Frame rendered
        private void Update()
        {
            // Find all GameObjects of Type Agent
            // This is a somewhat heavy function and could lag the game when more Agents are present
            // TODO: Implement Callback to get all Agents after spawn script has spawned them
            Agent[] allAgents = FindObjectsOfType<Agent>();

            // Go through each Agent and each Cargo it holds
            foreach (Agent agent in allAgents)
            {
                Debug.Log("Haul Length" + agent.TotalFreight.ToArray().Length);
                foreach (Haulable_I haulable in agent.TotalFreight)
                {
                    // Set the score based on Agent unique ID (Name not sufficient), the Type of Cargo and its Amount
                    Ware ware = (Ware)haulable;
                    Debug.Log("INSERT: " + agent.GetInstanceID().ToString() + ware.getWareType().ToString() + ware.getAmount());
                    this.setScore(agent.GetInstanceID().ToString(), haulable.getWareType().ToString(), ware.getAmount());
                }
                // Finally set the Score for the Agents Money
                this.setScore(agent.GetInstanceID().ToString(), "MONEY", agent.Wallet);
            }
        }

        
        // Gets called before each Score manipulation Method
        void Init()
        {
            // Check if a Score Dictionary exists. 
            // If not create Score Dictionary to prevent nullpointer exceptions
            if (agentScores != null)
            {
                return;
            }
            agentScores = new Dictionary<string, Dictionary<string, int>>();
        }

        // Get the Score value from agentname of scoreType
        public int getScore(string agentname, string scoreType)
        {
            Init();

            // Return 0 as Score when the agent is not in Score Dictionary 
            if (agentScores.ContainsKey(agentname) == false)
            {
                return 0;
            }
            // or has no value for scoreType
            if (agentScores[agentname].ContainsKey(scoreType) == false)
            {
                return 0;
            }

            return agentScores[agentname][scoreType];
        }

        // Set the Score value for scoreType of agentname to value
        public void setScore(string agentname, string scoreType, int value)
        {
            Init();
            // Check if agent has had a scoreType entry in the Score Dictionary, if not create it
            if (agentScores.ContainsKey(agentname) == false)
            {
                agentScores[agentname] = new Dictionary<string, int>();
            }

            // Set Score Value in Dictionary and set bool to true for ScoreList to trigger update
            agentScores[agentname][scoreType] = value;
            this.setHasChanged(true);
        }

        public void addScore(string agentname, string scoreType, int amount)
        {
            Init();
            // Get Score and add amount.
            // Save in Dictionary and set bool to true for ScoreList to trigger update
            int currentScore = getScore(agentname, scoreType);
            setScore(agentname, scoreType, currentScore + amount);
            this.setHasChanged(true);
        }

        // Get the names of all Agents currently present in Score Dictionary
        public string[] getAgentNames()
        {        
            return agentScores.Keys.ToArray();
        }

        // Get and Set for update bool
        // Used for ScoreList to trigger updates of UI Elements
        public bool getHasChanged()
        {
            return hasChanged;
        }

        public void setHasChanged(bool hasChanged)
        {
            this.hasChanged = hasChanged;
        }
    }
}
