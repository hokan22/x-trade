﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace XTrade
{
	public class DisplayHandler : MonoBehaviour
	{
		public GameObject infoDisplay;
		public Text infoText;

		private Asteroid Parent;

		// Use this for initialization
		void Start()
		{
			infoDisplay.SetActive(false);

			this.Parent = GetComponent<Asteroid>();
		}

		// Called when the mouse enters the GUIElement or Collider.
		void OnMouseEnter()
		{
			// Display Hovermenu
			infoDisplay.SetActive(true);

			// TODO: Change Text to current Value
			infoText.text = this.Parent.getWareTypeNiceName() + " " + this.Parent.getAmount();
		}

		// Called when the mouse is not any longer over the GUIElement or Collider.
		void OnMouseExit()
		{
			// Hide Hovermenu
			infoDisplay.SetActive(false);
		}
	}
}
