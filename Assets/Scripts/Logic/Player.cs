﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class Player : Agent
	{
		public Player()
		{
		}

		public override Decisions determineMarketplaceDecision()
		{
			throw new System.NotImplementedException();
		}

		public override List<Tradable_I> determineObjectsToTrade(List<Tradable_I> tradableObjects)
		{
			throw new System.NotImplementedException();
		}

		internal override void buildStation()
		{
			throw new System.NotImplementedException();
		}

		internal override TradeContract determineContractToBuy(List<TradeContract> contracts)
		{
			throw new System.NotImplementedException();
		}

		internal override int determineContractWareAmount()
		{
			throw new System.NotImplementedException();
		}

		internal override Haulable_I determineContractWareType()
		{
			throw new System.NotImplementedException();
		}

		internal override List<Haulable_I> determineFreightForExchange()
		{
			throw new System.NotImplementedException();
		}

		internal override void determineShipBehaviour(Ship ship, Task TASK, WareType WARETYPE)
		{
			throw new System.NotImplementedException();
		}

		internal override List<Haulable_I> determinFreightForExchange(List<Haulable_I> freight)
		{
			throw new System.NotImplementedException();
		}
	}
}
