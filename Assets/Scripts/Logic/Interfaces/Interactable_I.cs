﻿using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public interface Interactable_I
	{
		List<Interactions> getInteractOptions();
	}

	public enum Interactions
	{
		TRADEABLE,
		MINEABLE,
		ATTACKABLE,
		SHIPYARD,
	}
}
