﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	interface HasMarketplace_I
	{
		TradeManager getTradeManager();
	}
}
