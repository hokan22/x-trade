DROP DATABASE IF EXISTS `xtrade`;
CREATE DATABASE `xtrade` COLLATE utf8_general_ci;
USE `xtrade`;
CREATE TABLE `highscores` (
        `id` INT UNSIGNED ZEROFILL NOT NULL primary key AUTO_INCREMENT,
        `score` INT UNSIGNED ZEROFILL NOT NULL,
        `timestamp` INT UNSIGNED NOT NULL,
        `nick` VARCHAR(50) NOT NULL,
        `playtime` INT UNSIGNED NOT NULL
);
