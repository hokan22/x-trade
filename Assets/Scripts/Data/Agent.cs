﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace XTrade
{
	public abstract class Agent : MonoBehaviour
	{
		private int wallet;
		public int Wallet { get { return wallet; } set { wallet = value; } }

		private List<Haulable_I> totalFreight;
		public List<Haulable_I> TotalFreight { get { updateTotalFreight(); return totalFreight; } }

		private EntityManager entityManager;
		public EntityManager EntityManager { get { return entityManager; } set { entityManager = value; } }

		public Agent()
		{
		}

		public void Awake()
		{
			wallet = 10000;

			entityManager = new EntityManager();

			totalFreight = new List<Haulable_I>();

		}

		public abstract List<Tradable_I> determineObjectsToTrade(List<Tradable_I> tradableObjects);

		internal abstract List<Haulable_I> determineFreightForExchange();

		public abstract Decisions determineMarketplaceDecision();

		internal abstract Haulable_I determineContractWareType();

		internal abstract int determineContractWareAmount();

		internal abstract TradeContract determineContractToBuy(List<TradeContract> contracts);

		internal abstract List<Haulable_I> determinFreightForExchange(List<Haulable_I> freight);

		internal abstract void determineShipBehaviour(Ship ship, Task TASK, WareType WARETYPE);

		internal abstract void buildStation();

		public bool hasFiveStation()
		{
			List<SpaceEntity> listOfStations = entityManager.getAllEntitiesOfType(typeof(Station));

			return listOfStations.Count > 5 ? true : false;
		}

		private void updateTotalFreight()
		{
			if(EntityManager.AllEntities == null)
			{
				Debug.LogError("AllEntitiesLeer");
				return;
			}
			foreach(KeyValuePair<Type, List<SpaceEntity>> allEntitiesOfType in EntityManager.AllEntities)
			{
				if (allEntitiesOfType.Value == null)
				{
					Debug.LogError("EnttyValueLeer");
					return;
				}
				foreach (SpaceEntity entity in allEntitiesOfType.Value)
				{
					if (entity == null)
					{
						Debug.LogError("EntityInValueLeer");
						return;
					}
					if (entity is CanHaul_I)
					{

						CanHaul_I canHaul = (CanHaul_I)entity;
						totalFreight.AddRange(canHaul.getFreight());
					}
				}
			}
		}

		public bool reduceWareBy(WareType waretype, int amount)
		{

			List<Haulable_I> designatedWareList = new List<Haulable_I>();

			foreach(Haulable_I ware in TotalFreight)
			{
				if(ware.getWareType() == waretype)
				{
					designatedWareList.Add(ware);
				}
			}

			int reducedAmount = 0;

			Debug.Log("Reduce:" + waretype.ToString() + " by " + amount);

			foreach(Haulable_I ware in designatedWareList)
			{
				if(ware.GetType() == typeof(Ware))
				{
					Ware designatedWare = (Ware)ware;

					if (amount- reducedAmount > designatedWare.getAmount())
					{
						reducedAmount += amount;
						TotalFreight.Remove(ware);

					}
					// If the amountToSell does not exceed the Amount of the current Ware, it create a new Ware object, which holds the delta Amount of 
					// the ware to Sell and sells the current ware object.
					else
					{
						Ware deltaWare = new Ware(ware.getWareType(), designatedWare.getAmount() - amount);
						designatedWare.reduceAmountBy(designatedWare.getAmount() - amount);
						amount = 0;
						TotalFreight.Remove(ware);
						TotalFreight.Add(deltaWare);
						return true;
					}
				}


			}

			return false;
		}
	}

	// Every possible Decision in every menu or gameplay mechanic will be written down in this enum to have a central place for agent decisions
	public enum Decisions
	{
		NEW_CONTRACT = 0,
		BUY_CONTRACT = 1,
		LEAVE_MARKETPLACE = 2

	}
}
