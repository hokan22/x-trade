﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class CurrentTradablePrice
	{
		private Haulable_I wareType;
		public Haulable_I WareType { get { return wareType; } set { wareType = value; } }
		private int price;
		public int Price { get { return price; } set { price = value; } }

		public CurrentTradablePrice(Haulable_I type, int price)
		{
			wareType = type;
			this.price = price;
		}
	}
}
