﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XTrade
{
	public class TradeManager
	{
		private List<TradeContract> contracts;
		public List<TradeContract> Contracts { get { return contracts; }  set { contracts = value; } }

		public TradeManager()
		{
			contracts = new List<TradeContract>();
		}

		public void addContract(TradeContract contract)
		{
			contracts.Add(contract);
		}
	}
}
